import React from 'react';

import './Nav.css';



const Navbar = () => (
    <nav className="nav-wrapper">
        <div className="logo"> LO_GO_GO</div>
        <div className="search">SEARCH

        </div>
        <div className="user-section">
            <div className="favourite"> favou</div>
            <div className="shopping-card">shopping-card</div>
            <div className="user-account">
                <h3>Username</h3>
                <div className="user-avatar"></div>
            </div>
        </div>
    </nav>
    )

export default Navbar;