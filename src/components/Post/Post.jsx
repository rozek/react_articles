import React from 'react';

import './Post.css';

const Post = ({post: {title, img, body, author}, i}) => (
    <div className="post-container">
        <h1 className="heading">{title}</h1>
        <img className="image" src={img} alt="post"/>
        <p> {body}</p>
        <div className="info">
<h6>Article number: {i+1}</h6>
            <h5>Written by: {author}</h5>
        </div>
    </div>
    )

export default Post;