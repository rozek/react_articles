import React, { Component } from 'react';

import './App.css';

import Nav from './components/layout/Nav/Nav';
import Posts from './components/Posts/Posts';


const App = () => (
        <div className='main-container'>
            <div>
                <Nav />
            </div>
           <h1 className='main-heading'>Breaking News</h1>
            <Posts />
        </div>
    );

export default App;